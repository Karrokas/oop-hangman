import java.io.IOException;
import java.util.Scanner;

public class Peaklass {
    public static void main(String[] args) throws IOException {

        Scanner sc = new Scanner(System.in);

        System.out.println("Hei! Olete jõudnud mängima poomismängu, kus tuleb ära arvata, mis sõna aruvti genereerinud on.");
        System.out.println("Mängu alustamiseks jooksutage programmi ning vajutage seejärel terminalile, et sisestada soovitud teema.");
        System.out.println("Teil on kokku kuus katset ning iga lõppenud katse lõpus küsitakse Teilt uuesti, kas soovite veel ühe mängu mängida.");
        System.out.println();
        System.out.println("Palju edu!");
        System.out.println();

        boolean KasSaSoovidMängida = true;
        while (KasSaSoovidMängida) {

            Hangman mäng = new Hangman();
            do {
                System.out.println(mäng.joonistused());
                System.out.println(mäng.hetkeneArvamine());

                System.out.println("Sisestage suvaline täht, mida arvate sõnas olevat:");
                String arvamine = (sc.next().toLowerCase());
                if (arvamine.length()==1) {
                    char arvamine2 = arvamine.charAt(0);
                    while (mäng.jubaArvatud(arvamine2)) {
                        System.out.println("Proovige uuesti, olete juba seda tähte arvanud.");
                        arvamine2 = (sc.next().toLowerCase()).charAt(0);
                    }
                    if (mäng.kasSisaldub(arvamine2)) {
                        System.out.println("See täht on olemas ");
                        System.out.println("-------------------");
                    }
                    else {
                        System.out.println("Kahjuks seda tähte seal pole.");
                        System.out.println("-------------------");
                    }
                }
                else{
                    System.out.println("Te sisestasite liiga palju tähti korraga. Sisestage palun üks täht.");
                }

            }
            while (!mäng.mängläbi());


            System.out.println("Kas sooviksite ühe mängu veel teha?");
            System.out.println( "Kui jah, siis sisestage 'j'. Kui ei, siis vajutage suvalisele klahvile.");
            Character vastusKusimusele = (sc.next().toLowerCase().charAt(0));
            KasSaSoovidMängida = (vastusKusimusele == 'j');
        }
    }



}
