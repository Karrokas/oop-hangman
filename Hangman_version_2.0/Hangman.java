package oop;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Scanner;

public class Hangman {
   private String juhuslikSõna;
   private StringBuilder hetkeArvamus;
   private int katseteArv = 6;
   public int hetkeseis = 0;

    ArrayList<Character> eelmineArvamus = new ArrayList<>();
    ArrayList<String> sõnadeKogumik = new ArrayList<>();


    public Hangman() throws IOException{
        käivitaSõnad();
        juhuslikSõna = valiSõna();
        hetkeArvamus = joonedJaTühikud();
    }





    public void  käivitaSõnad() throws IOException{
       Scanner TekstFail = new Scanner(System.in);
       // System.out.println("Palun vali kolme kategooria vahel: Loomad, Masinad, Automargid ");
        String kasutajaValik = "D:\\Kool\\Loomad.txt";

//        if (kasutajaValik.equals("Loomad")){
//            kasutajaValik = "D:\\Kool\\Loomad.txt";
//        }
//        else if (kasutajaValik.equals("Masinad")){
//            kasutajaValik = "Masinad.txt";
//        }
//        else if (kasutajaValik.equals("Automargid")){
//            kasutajaValik = "Automargid.txt";
//        }


        File fail = new File(kasutajaValik);
        try(Scanner scan = new Scanner(fail,"UTF-8")) {
            while (scan.hasNextLine()){
                String sõna = scan.nextLine();
                sõnadeKogumik.add(sõna);
            }
        }
        catch (IOException i){
            System.out.println("Pole sõnu mida lugeda");

        }
    }


    public String valiSõna(){
        int sõnaIndeks = (int)(Math.random()*sõnadeKogumik.size());
        return sõnadeKogumik.get(sõnaIndeks);
    }


    public StringBuilder joonedJaTühikud() {
        StringBuilder jooned = new StringBuilder();
        for (int i = 0; i < juhuslikSõna.length()*2; i++) {
            if (i % 2 == 0) {
                jooned.append("_");
            } else {
                jooned.append(" ");
            }
        }
        return jooned;
    }


    public String hetkeneArvamine() {
        return "Hetkene arvamine : " + hetkeArvamus.toString();//jooned ja tühikud tostringi
    }

    public boolean mängläbi() {
        if (võit()) {
            System.out.println();
            System.out.println("Arvasite sõna ära. Olite tubli!");
            System.out.println("Õige sõna oligi "+juhuslikSõna+".");
            return true;
        }
        else if (kaotus()) {
            System.out.println();
            System.out.println("Vabandust, aga kõik Teie 6 katset on läbi. Õige sõna oli " + juhuslikSõna + ".");
            return true;
        }
        return false;
    }

    public boolean kaotus() {
        return hetkeseis >= katseteArv;
    }

    public boolean võit() {
        String arvamus = lühendatudArvamus();
        return arvamus.equals(juhuslikSõna);
    }

    public String lühendatudArvamus() {
        String arvamus = hetkeArvamus.toString();
        return arvamus.replace(" ", "");

    }


    public boolean jubaArvatud(char element) {
        return eelmineArvamus.contains(element);
    }

    public boolean kasSisaldub(char element) {
        boolean KasArvasidOigesti = false;

        eelmineArvamus.add(element);
        for (int i = 0; i < juhuslikSõna.length(); i++) {
            if (juhuslikSõna.charAt(i) == Character.toUpperCase(element) || juhuslikSõna.charAt(i) == element) {
                if (juhuslikSõna.charAt(i) == Character.toUpperCase(element)){

                    hetkeArvamus.setCharAt(i * 2,Character.toUpperCase(element));

                    KasArvasidOigesti = true;}
                else if (juhuslikSõna.charAt(i) == element){
                    hetkeArvamus.setCharAt(i*2,element);
                    KasArvasidOigesti=true;

                }
            }
        }
        if (!KasArvasidOigesti) {
            hetkeseis++;

        }

        return KasArvasidOigesti;
    }

    public String joonistused() {
        switch(hetkeseis) {
            case 0: return ainultPost();
            case 1: return postPeaga();
            case 2: return postKehaga();
            case 3: return postKäega();
            case 4: return postTeiseKäega();
            case 5: return postJalaga();
            default: return viimane();
        }

    }

    private String ainultPost() {
        return " - - - - -\n"+
                "|        |\n"+
                "|        \n" +
                "|       \n"+
                "|        \n" +
                "|       \n" +
                "|\n" +
                "|\n";
    }

    private String postPeaga() {
        return " - - - - -\n"+
                "|        |\n"+
                "|        O\n" +
                "|       \n"+
                "|        \n" +
                "|       \n" +
                "|\n" +
                "|\n";
    }

    private String postKehaga() {
        return " - - - - -\n"+
                "|        |\n"+
                "|        O\n" +
                "|        | \n"+
                "|        |\n" +
                "|        \n" +
                "|\n" +
                "|\n";
    }

    private String postKäega() {
        return   " - - - - -\n"+
                "|        |\n"+
                "|        O\n" +
                "|      / |  \n"+
                "|        |\n" +
                "|        \n" +
                "|\n" +
                "|\n";
    }

    private String postTeiseKäega() {
        return  " - - - - -\n"+
                "|        |\n"+
                "|        O\n" +
                "|      / | \\ \n"+
                "|        |\n" +
                "|        \n" +
                "|\n" +
                "|\n";
    }

    private String postJalaga() {
        return   " - - - - -\n"+
                "|        |\n"+
                "|        O\n" +
                "|      / | \\ \n"+
                "|        |\n" +
                "|       / \n" +
                "|\n" +
                "|\n";
    }

    private String viimane() {
        return   " - - - - -\n"+
                "|        |\n"+
                "|        O\n" +
                "|      / | \\ \n"+
                "|        |\n" +
                "|       /  \\ \n" +
                "|\n" +
                "|\n";
    }
}
